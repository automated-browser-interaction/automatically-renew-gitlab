const { defineConfig } = require('cypress')

module.exports = defineConfig({
  fixturesFolder: false,
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    embeddedScreenshots: true,
    inlineAssets: false
  },
  e2e: {
    supportFile: false,
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);
    },
  },
})
