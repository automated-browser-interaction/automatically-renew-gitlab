describe('Renew', () => {
  // https://docs.gitlab.com/ee/subscriptions/#meeting-gitlab-for-open-source-program-requirements
  it("Takes a screenshot of your project's overview page showing the LICENSE", () => {
    cy.visit(Cypress.env('CI_PROJECT_URL'));
    const licenseLink = cy.get('a[itemprop="license"]'); // wait for at least this to load
    // licenseLink.should('have.attr', 'href'); cannot assert twice
    const licenseBranch = Cypress.env('CI_DEFAULT_BRANCH')
    licenseLink.should('have.attr', 'href', '/'+Cypress.env('CI_PROJECT_PATH')+'/-/blob/'+licenseBranch+'/LICENSE');
    // const licenseFileLink = cy.get('a[data-original-title="LICENSE"]');
    const licenseFileLink = cy.get('a[title="LICENSE"]');
    licenseFileLink.should('have.attr', 'href', '/'+Cypress.env('CI_PROJECT_PATH')+'/-/blob/'+licenseBranch+'/LICENSE');
    cy.get('nav[data-testid="breadcrumb-links"]').eq(0).invoke('prop', 'innerHTML').then((doc) => {
      cy.writeFile('../public/breadcrumb-links.html', doc);
    });
    // https://stackoverflow.com/questions/58720705/how-to-change-the-documents-html-of-the-app-under-test-aut
    // Because Cypress isn't logged in, it views the repo as a member of the public, so it won't see the renewal banner.
    //cy.get('div[data-feature-id="ultimate_feature_removal_banner"]').eq(0).invoke('prop', 'innerHTML').then((doc) => {
    //  cy.writeFile('../public/ultimate_feature_removal_banner.html', doc);
    //});
    // https://filiphric.com/waiting-in-cypress-and-how-to-avoid-it
    licenseFileLink.wait(2000);
    cy.screenshot('project-overview-showing-license', {capture: 'viewport'});
  })

  it("Takes a screenshot of the contents of your project's LICENSE", () => {
    const licenseBranch = Cypress.env('CI_DEFAULT_BRANCH')
    const licenseURL = Cypress.env('CI_PROJECT_URL')+'/-/blob/'+licenseBranch+'/LICENSE';
    cy.visit(licenseURL);
    cy.get('div[data-path="LICENSE"]').wait(2000); // Even after you wait for the LICENSE banner,
    // GitLab does this weird thing where it prints the text again over itself.
    // https://filiphric.com/waiting-in-cypress-and-how-to-avoid-it
    cy.screenshot('license-contents');
  })

  it("Takes a screenshot of your project's visibility settings", () => {
    const settingsURL = Cypress.env('CI_PROJECT_URL')+'/edit';
    //cy.visit(settingsURL);
    //cy.get('h4[class="settings-title"]').contains('Visibility, project features, permissions').click();
    // assume the settings are already correct, otherwise you wouldn't be running this
    //cy.screenshot('project-visibility');
    // the fundamental problem here is that the CI_JOB_TOKEN can do a lot through APIs but cannot view the settings in the browser
    // could leave this as a remaining manual step, take this screenshot manually...
    // ...but how to coolect it? save it in the project repo? that's really ugly
    // Cypress *could* authenticate in the browser. Cypress is really good at that.
    // But it would need credentials, real credentials, and saving those as variables smells of bad practice.
    cy.visit('./SettingsComplete.html');
    cy.get('h4');
    //cy.get('h4[class="settings-title"]').contains('Visibility, project features, permissions').click();
    cy.screenshot('project-visibility', {capture: 'viewport'});
  })

  it('renews', () => {
    cy.visit('https://offers.sheerid.com/gitlab/member/');
    cy.get('html:root').eq(0).invoke('prop', 'innerHTML').then((doc) => {
      cy.writeFile('../public/pageMarkup.html', doc);
    });
    cy.get('div[class="sid-verification-form"]');
    const names = Cypress.env('GITLAB_USER_NAME').split(' ');
    cy.get('input[name="sid-first-name"]').type(names[0]);
    expect(names.slice(-1)[0]).to.be.a('string');
    cy.get('input[name="sid-last-name"]').type(names.slice(-1)[0]);
    cy.get('input[name="sid-email"]').type(Cypress.env('GITLAB_USER_EMAIL'));
    cy.get('input[id="sid-country"]').type(Cypress.env('PRIMARY_COUNTRY'));
    //const nice_project_name = Cypress.env('CI_PROJECT_NAME').split('-')
    //  .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    //  .join(' ');
    const nice_project_name = Cypress.env('CI_PROJECT_TITLE')
    cy.get('input[name="sid-project-name"]').type(nice_project_name);
    expect(Cypress.env('CI_PROJECT_DESCRIPTION').length).to.be.at.least(2);
    expect(Cypress.env('CI_PROJECT_DESCRIPTION').length).to.be.at.most(256);
    cy.get('textarea[name="project-description"]').type(Cypress.env('CI_PROJECT_DESCRIPTION'));
    expect(Cypress.env('CI_PROJECT_URL').length).to.be.at.most(256);
    cy.get('textarea[name="sid-public-project"]').type(Cypress.env('CI_PROJECT_URL'));
    const licenseBranch = Cypress.env('CI_DEFAULT_BRANCH')
    const licenseURL = Cypress.env('CI_PROJECT_URL')+'/-/blob/'+licenseBranch+'/LICENSE';
    // check Cypress.env('CI_PROJECT_URL')+'/-/raw/'+licenseBranch+'/LICENSE'
    expect(licenseURL.length).to.be.at.most(256);
    cy.get('textarea[name="sid-osi-license"]').type(licenseURL);
    cy.get('div[class="sid-field__label sid-checkbox__label"]').each((item, index, list) => {
      expect(index).to.be.at.most(1);
      // https://docs.cypress.io/api/commands/children#Command-Log
      // cy.get('.left-nav>.nav').children().should('have.length', 8)
      // but item.children().should('have.length', 1); errors out with TypeError: item.children(...).should is not a function
      item.children()[0].text;
      // throw new Error(Object.prototype.toString.call(item.children()[0]));
      expect(Object.prototype.toString.call(item.children()[0])).to.equal('[object HTMLSpanElement]');
      expect(item.children()[0].innerText).to.be.a('string');
      const expectedText = 'I certify that project maintainers are not seeking to make profit from this project by, for example, selling services or higher tiers. Make sure your use case qualifies.'
      if (index == 0) {
        expect(item.children()[0].innerText).to.equal(expectedText);
        const labelElement = item.parent();
        expect(typeof labelElement).to.equal('object');
        expect(Object.prototype.toString.call(labelElement)).to.equal('[object Object]');
        const checkboxDiv = labelElement.children()[0];
        expect(Object.prototype.toString.call(checkboxDiv)).to.equal('[object HTMLDivElement]');
        const checkboxInput = checkboxDiv.children[0];
        expect(Object.prototype.toString.call(checkboxInput)).to.equal('[object HTMLInputElement]');
        expect(checkboxInput.type).to.equal('checkbox');
        checkboxInput.click();
        cy.screenshot('not-seeking-profit');
      }
    });
    // const dryRun = Boolean(Cypress.env('DRY_RUN_RENEW_GITLAB')); // any nonempty string will coerce to true; this is desired
    //if (dryRun) {
      // emit some kind of message noting this is a dry run?
    //}
    //else {
      // actually submit
    //}
    // better to turn it around and you have to define a variable to actually submit
    const actuallySubmit = (Cypress.env('TIME_TO_RENEW_GITLAB_SUBMIT') === "true");
    if (actuallySubmit) {
      // turns out something about the process starts before you submit the screenshots: if you then exit without submitting the screenshots, you'll get an email
      // so even the first submit button should not be clicked on a dry run
      // (hopefully all that testing earlier didn't go to any humans...)
      cy.get('button[type="submit"]').click();
      cy.screenshot('submitted');
      const fileInput = cy.get('input[type="file"]');
      cy.screenshot('file-input');
      // Strangely, the screenshot upload has pointer-events: none set.
      // Fortunately (?), when you do the process manually, most browsers seem to ignore this.
      // Cypress rightly complains that pointer-events: none` prevents user mouse interaction.
      fileInput.should('have.attr', 'style', 'position: absolute; inset: 0px; opacity: 1e-05; pointer-events: none;');
      const fileInputWithoutStyle = fileInput.then(function($input){
        $input[0].setAttribute('style', 'position: absolute; inset: 0px; opacity: 1e-05;')
      });
      fileInputWithoutStyle.selectFile(['cypress/screenshots/renew.cy.js/project-overview-showing-license.png', 'cypress/screenshots/renew.cy.js/license-contents.png']);
      const submitButton = cy.get('button[id="sid-submit-doc-upload"]');
      cy.screenshot('uploaded-screenshots');
    }
  });
});
