ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=automated-browser-interaction
ARG DOCKER_BASE_IMAGE_NAME=cypress-curl-wget-image
ARG DOCKER_BASE_IMAGE_TAG=18.12.1
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

COPY package.json .

RUN set -o allexport && \
    . ./fix_all_gotchas.sh && \
    set +o allexport && \
    echo "NODE_PATH=$NODE_PATH" && \
    echo "npm root --global" && \
    npm root --global && \
    # In Node.js, require doesn't look in the folder where global modules are installed.
    # That is, npm install --global does not install globally.
    # No, seriously.
    # https://stackoverflow.com/questions/15636367/nodejs-require-a-global-module-package
    # https://github.com/nodejs/node/issues/22922
    # As far as I can tell, there is no "really truly globally install for realsies" command. You have to manually hack the paths yourself.
    # I don't know *what* npm link does, but it sure doesn't cause globally installed packages to be found.
    # Incredibly, even manually hacking NODE_PATH, npm install --global *still* doesn't work, because npm install --global skips the dependencies.
    # This anti-feature is, of course, completely undocumented.
    # https://docs.npmjs.com/cli/v9/commands/npm-install#global
    # So the only option is to hack together your own "--global" by skipping --global and pointing NODE_PATH at wherever you installed.
    # du --human-readable --max-depth=1 $(npm root --global) && \
    # npm install --global && \
    # du --human-readable --max-depth=1 $(npm root --global) && \
    # npm list --global dragula && \
    pwd && \
    if [ -d node_modules ]; then (echo "Something weird is going on: node_modules is already there." && exit 1); fi && \
    npm install && \
    npm cache clean --force && \
    du --human-readable --max-depth=0 node_modules && \
    npm list lodash && \
    npm list typescript && \
    # We don't actually want the package itself installed...
    # npm list cypress-example-recipes && \
    . ./cleanup.sh

ENV NODE_PATH /node_modules
