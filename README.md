### Automatically renew GitLab

For background, as explained at https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/#must-members-of-the-gitlab-for-open-source-program-renew-their-memberships:

> Program members must renew their memberships annually.
>
> We recommend that applicants begin the renewal process at least one month in advance of their renewal dates to ensure sufficient processing time.

(Obviously that last bit is mostly in case of a processing bottleneck, but it's also very easy to screw up one of the steps, in which case, *first* you won't hear back for a while (due to the *normal* processing delays), and *then* you'll have to start over from the beginning.)

You may be interested in this project if you've ever run into any of the following issues:

+ Messing up one of the fiddly browser-screenshot steps.
+ Pasting the description of an entirely different project in. (...there really is no excuse.)
+ Entering the wrong email address. (The process will only work in the end if the given email address is the *primary* email address of an account that is an Owner at the *group* level.)
+ Not knowing whether someone has already submitted the renewal or not.

The primary purpose of this job is to prevent errors. (Errors force *you* to start over, and they also waste the time of the admins on the backend, which increases *everyone's* processing delays.)

The secondary purpose is for record-keeping. If you aren't sold on the advantages of *that*, pause to reflect that any necessary conversations with the admins will go much more smoothly if you have in your hands the exact screenshots from the original submission. (You may think that you'll remember the details weeks later, but you won't.)

A tertiary benefit of that record-keeping is simply knowing whether someone already submitted the renewal for your project, so you don't submit a redundant application.

To use this job in your project, simply include it in your .gitlab-ci.yml:

    include:
    - project: 'automated-browser-interaction/automatically-renew-gitlab'
      file: 'automatic_renew.yaml'
      ref: main

Then, when it's time to renew, run a pipeline and set the variable TIME_TO_RENEW_GITLAB. (It doesn't matter what you renew it to.)

You can set the variable DRY_RUN_RENEW_GITLAB to take all the screenshots but not actually submit the application, just so you can review the screenshots to double-check that your information is still accurate. (Or, more accurately, so that someone who isn't 100% sure of the information can send screenshots to someone who *is* to glance over.)

~~The screenshots can be saved as GitLab artifacts, but those are easy to lose, so it's recommended to provide an SSH_PRIVATE_DEPLOY_KEY and a REGISTRY_TO_SAVE_SCREENSHOTS, which will be used to save the records of your application.~~ Not implemented yet. Note however that GitLab artifacts will stick around indefinitely as long as the particular job is not re-run. It might be better to misuse the package registry for record-keeping; watch this space.
